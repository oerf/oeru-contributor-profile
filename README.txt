To test this, simply check it out with Git into a suitable directory:

cd [some suitable directory on your computer]
git clone https://kiwilightweight@bitbucket.org/oerf/oeru-contributor-profile.git

and then, to view it in your browser's URL bar enter:

file://[some suitable directory on your computer]/oeru-contributor-profile/index.html

Note, if your directory path is absolute, i.e. it starts with a "/", e.g. /var/www, your file might start with ///, e.g. file:///var/www/oeru-contributor-profile/index.html - don't let that trouble you :) 
